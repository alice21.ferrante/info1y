#include <stdio.h>
#include <string.h>
#define LEN 35
int compare(char s1[], char s2[]){  /*realizzo un sottoprogramma iniziale, utile a verificare se due stringhe sono compatibili secondo le specifiche*/
	int i, len1, len2, ok;
	
	len1=strlen(s1);          
	len2=strlen(s2);     
	
	if(len1!=len2){     /*se le due stringhe hanno lunghezza diversa, sicuramente non sono compatibili*/
		return 0;
	}
    
    ok=1;
	for(i=0; i<len1 && ok==1; i++){       /*scorro ogni carattere della stringa*/
		if(s1[i]!=' ' && s1[i]!=s2[i]){   /*i caratteri della stringa s1 diversi dagli spazi vuoti devono essere uguali a quelli di s2(nella stessa posizion), se ci� non accade...*/
			ok=0;      /*...ok=0, esco dal ciclo e le due stringhe non sono compatibili, return ok=0;*/
		}
	}
    return ok;        /*in caso contrario il valore di ok non verr� mai modificato e restituisco ok=1, stringhe compatibili*/
}

int compatibili(char schema[], FILE *fp){
	char vocabolo[LEN+1];     /*il file non lo apro, ricevo solo il suo riferimento, neanche so il nome; se non lo apro, non lo chiudo nemmeno*/
	int cont;
	
	cont=0;
	while(fscanf(fp, "%s", vocabolo)!=EOF){  /*analizzo ogni parola del file*/
		if(compare(schema, vocabolo)){       /*verifico la compatibilit� delle parola del file con la stringa schema, mediante l'utilizzo del sottoprogramma prima realizzato*/
			printf("\n%s", vocabolo);      /*in caso positivo stampo la parola del file...*/
			cont++;                        /*...e aumento il conteggio delle parole*/
		}	
	}
	return cont;
}

