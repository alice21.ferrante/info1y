#include <stdio.h>
#include <string.h>

typedef struct {
	
	int giorno;
    int mese;
	int anno; 
	
	} Data;

/* Stampa la data nel formato gg/mm/aaaa */
	
void stampa( Data d ) 
{

	Data * punt_d = &d;	
  
	printf("%d/%d/%d\n",punt_d->giorno,punt_d->mese,punt_d->anno);
}

/* Stabilisce se due date, passate come parametri, sono uguali (nel qual caso 
   restituisce 0), oppure in ordine cronologico (e allora restituisce un numero 
	negativo), oppure in ordine cronologico inverso (restituendo un numero positivo) */
int datecmp( Data d1, Data d2 )
{   
	
	Data * punt_d1 = &d1;
	Data * punt_d2 = &d2;
	
	if (punt_d1->anno < punt_d2->anno) {
		return -1;
	}
		
			
	else if (punt_d1->anno > punt_d2->anno) {
		return 1;
	}
	
	else if (punt_d1->anno == punt_d2->anno)		
	{
			
		if (punt_d1->mese < punt_d2->mese) {
			return -1;	
		}
			
		else if (punt_d1->mese > punt_d2->mese) {
			return 1;	
		}
					
		else if (punt_d1->mese == punt_d2->mese)
		{
				
			if (punt_d1->giorno < punt_d2->giorno) {
				return -1;	
			}
			
			else if (punt_d1->giorno > punt_d2->giorno) {
				return 1;	
			}
				
			else if (punt_d1->giorno == punt_d2->giorno)
			{
					return 0;
			}
				
		}
	
			
	}
	
	else {
		
		printf("ERROR\n");
		return 2;
	}
		
}


int main(void)
{

  Data a = { 15,  3,  -43},  /* Tu quoque! */
       b = { 12, 10, 1492},  /* I'd like to have a hamburger! */
       c = { 31, 10, 2013},  /* Marry octal Christmas! */
       d = { 31, 10, 2014},  /* And once more */
       e = { 30,  9, 2015},  /* Happy birthday, Reiko Shiota! */
       f = { 17,  1, 2018},  /* Ragnarök */
       g = { 14,  2, 2018};  /* Chocolate! */

  /* Ovviamente a < b < c < d < e < f < g */

  printf("\n a) : ");
  stampa(a);
  printf("\n b) : ");
  stampa(b);
  printf("\n c) : ");
  stampa(c);
  printf("\n d) : ");
  stampa(d);
  printf("\n e) : ");
  stampa(e);
  printf("\n f) : ");
  stampa(f);
  printf("\n\n");

  printf(" (a,b) -> %d\n", datecmp(a,b));	//-1
  printf(" (a,c) -> %d\n", datecmp(a,c));	//-1
  printf(" (a,d) -> %d\n", datecmp(a,d));	//-1
  printf(" (b,d) -> %d\n", datecmp(b,d));	//-1
  printf(" (c,d) -> %d\n", datecmp(c,d));	//-1
  printf(" (d,e) -> %d\n", datecmp(d,e));	//-1
  printf(" (b,b) -> %d\n", datecmp(b,b));	//0
  printf(" (c,c) -> %d\n", datecmp(c,c));	//0
  printf(" (d,d) -> %d\n", datecmp(d,d));	//0
  printf(" (f,d) -> %d\n", datecmp(f,d));	//1
  printf(" (f,b) -> %d\n", datecmp(f,b));	//1
  printf(" (e,d) -> %d\n", datecmp(e,d));	//1
  printf(" (e,b) -> %d\n", datecmp(e,b));	//1
  printf(" (d,c) -> %d\n", datecmp(d,c));	//1
  printf(" (d,b) -> %d\n", datecmp(d,b));	//1
  printf(" (c,b) -> %d\n", datecmp(c,b));	//1
  printf(" (b,a) -> %d\n", datecmp(b,a));	//1

  /*printf("\n a) : ");
  stampaLettere(a);
  printf("\n b) : ");
  stampaLettere(b);
  printf("\n c) : ");
  stampaLettere(c);
  printf("\n d) : ");
  stampaLettere(d);
  printf("\n e) : ");
  stampaLettere(e);
  printf("\n f) : ");
  stampaLettere(f);*/

	return 0;
}

