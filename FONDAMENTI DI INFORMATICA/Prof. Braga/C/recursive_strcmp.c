#include <stdio.h>
#include <string.h>

#define max 20

int rec_strcmp ( char * s1, char * s2 )
{
	
	if (*s1 == *s2)
	{
		if (*s1 != '\0' && *s2 != '\0' )
		{
			return rec_strcmp ((s1+1),(s2+1));	
		}
		
		return 0;
		
	}
	
	else if (*s1 < *s2)
	{
		return -1;	
	}
	
	else if (*s1 > *s2)
	{
		return 1;	
	}

	return 2;
	
}

int main (void)
{
	char s1 [max];
	char s2 [max];
	
	scanf("%s%s",s1,s2);
	
	printf("%d\n",rec_strcmp(s1,s2));
	
	return 0;
}